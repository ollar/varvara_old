from aiohttp import web

@web.middleware
async def cors_middleware(request, handler):
    configs = request.app['configs']

    headers = {
        'Access-Control-Allow-Origin': configs.app_headers_allow_origin,
        'Access-Control-Allow-Credentials': configs.app_headers_allow_credentials,
        'Access-Control-Allow-Headers': configs.app_headers_allow_headers,
        'Access-Control-Allow-Methods': configs.app_headers_allow_methods,
    }

    if request.method == 'OPTIONS':
        response = web.Response()
    else:
        response = await handler(request)
    response.headers.update(headers)

    return response