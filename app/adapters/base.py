class NotImplementedException(Exception):
    """This method is not implemented"""

class DatabaseRequired(Exception):
    """No db param provided"""

class RequestNotAcknowledged(Exception):
    """Mongo request failed for some reason"""

class Adapter():
    async def create_record(self, *args, **kwargs):
        raise NotImplementedException

    async def delete_record(self, *args, **kwargs):
        raise NotImplementedException

    async def find_all(self, *args, **kwargs):
        raise NotImplementedException

    async def find_record(self, *args, **kwargs):
        raise NotImplementedException

    async def update_record(self, *args, **kwargs):
        raise NotImplementedException
