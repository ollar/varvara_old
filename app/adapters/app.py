from bson import ObjectId
from .base import Adapter, DatabaseRequired, RequestNotAcknowledged
from pymongo.results import InsertOneResult, InsertManyResult


class AppAdapter(Adapter):
    list_length = 10
    def __init__(self, db, configs):
        if not db: raise DatabaseRequired
        self.db = db
        self.configs = configs


    async def find_record(self, collection_name, entity_id):
        return await self.db[collection_name].find_one({"_id": ObjectId(entity_id)})


    async def find_all(self, collection_name, find:dict={}, pagination=None):
        result = self.db[collection_name]\
                    .find(find)

        if pagination:
            result = result.skip((pagination - 1) * self.list_length)
        return await result.to_list(length=self.list_length)


    async def create_record(self, collection_name, data: object):
        result = None

        if type(data) is list:
            _mongo_res = await self.db[collection_name].insert_many(data)
        else:
            _mongo_res = await self.db[collection_name].insert_one(data)

        if not _mongo_res.acknowledged:
            raise RequestNotAcknowledged

        if type(_mongo_res) is InsertOneResult:
            result = await self.find_record(collection_name, _mongo_res.inserted_id)
        elif type(_mongo_res) is InsertManyResult:
            result = [await self.find_record(collection_name, _id) for _id in _mongo_res.inserted_ids]

        return result


    async def update_record(self, collection_name, entity_id, data):
        _mongo_res = await self.db[collection_name].replace_one({"_id": ObjectId(entity_id)}, data)

        if not _mongo_res.acknowledged:
            raise RequestNotAcknowledged

        return await self.find_record(collection_name, entity_id)


    async def patch_record(self, collection_name, entity_id, data):
        _mongo_res = await self.db[collection_name].update_one(
            {"_id": ObjectId(entity_id)},
            {"$set": data}
        )

        if not _mongo_res.acknowledged:
            raise RequestNotAcknowledged

        return await self.find_record(collection_name, entity_id)


    async def delete_record(self, collection_name, entity_id):
        _mongo_res = await self.db[collection_name].delete_one({"_id": ObjectId(entity_id)})

        if not _mongo_res.acknowledged:
            raise RequestNotAcknowledged

        return { "result": _mongo_res.acknowledged }
