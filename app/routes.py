from aiohttp import web
from functools import wraps

from hashlib import blake2b
import jwt
import json
import pymongo

from pymongo.errors import DuplicateKeyError
from .adapters.base import RequestNotAcknowledged
from .utils import json_dumps

from .errors import REQUEST_NOT_ACKNOWLEDGED, \
                    REQUIRED_FIELDS_NOT_SPECIFIED, \
                    PASSWORD_TOO_SHORT, \
                    DUPLICATE_KEY_ERROR, \
                    WRONG_CREDENTIALS

routes = web.RouteTableDef()


def mongo_errors_handler(func):
    @wraps(func)
    async def wrapper(*args, **kwargs):
        try:
            result = await func(*args, **kwargs)

        except DuplicateKeyError:
            return web.json_response(data={
                "error": DUPLICATE_KEY_ERROR
            }, status=400)

        except RequestNotAcknowledged:
            return web.json_response(data={
                "error": REQUEST_NOT_ACKNOWLEDGED
            }, status=500)

        return result

    return wrapper


def fields_validator(func):
    @wraps(func)
    async def wrapper(request):
        configs = request.app['configs']
        data = await request.json()

        if not data or len(data.keys()) == 0 or \
            not 'password' in data.keys() or \
            not set(configs.user_primary_fields).issubset(set(data.keys())):
            return web.json_response(data={
                "error": REQUIRED_FIELDS_NOT_SPECIFIED
            }, status=400)

        if len(data['password']) < 3:
            return web.json_response(data={
                "error": PASSWORD_TOO_SHORT
            }, status=400)

        return await func(request)

    return wrapper


@routes.post('/register')
@mongo_errors_handler
@fields_validator
async def register_handler(request):
    adapter = request.app['adapter']
    configs = request.app['configs']

    data = await request.json()
    user = dict(data)

    password = blake2b(data.get('password').encode(), key=configs.blake_key, salt=configs.blake_sault).hexdigest()

    user.update(password=password)
    user = await adapter.create_record('users', user)

    del user['password']
    _jwt = jwt.encode(json.loads(json_dumps(user)), key=configs.secret).decode()

    return web.json_response(data={"jwt": _jwt})


@routes.post('/authenticate')
@fields_validator
async def authenticate_handler(request):
    adapter = request.app['adapter']
    configs = request.app['configs']

    data = await request.json()

    password = blake2b(data.get('password').encode(), key=configs.blake_key, salt=configs.blake_sault).hexdigest()

    del data['password']

    query = {
        "$or": [{k: v} for k,v in data.items()]
    }

    users = await adapter.find_all('users', query)
    if len(users) == 0:
        return web.json_response(data={
            "error": WRONG_CREDENTIALS
        }, status=400)

    if len(users) > 1:
        raise web.json_response(data={
            "error": DUPLICATE_KEY_ERROR
        }, status=400)

    user = users[0]

    if user['password'] != password:
        return web.json_response(data={
            "error": WRONG_CREDENTIALS
        }, status=400)

    del user['password']
    _jwt = jwt.encode(json.loads(json_dumps(user)), key=configs.secret).decode()

    return web.json_response(data={"jwt": _jwt})


# maybe in future
@routes.post('/invalidate')
async def invalidate_handler(request):
    return web.json_response(data={})


def getRoutes():
    return routes