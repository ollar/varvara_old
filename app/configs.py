from settings import settings

class Configs():
    app_name = 'varvara'
    app_headers_allow_origin = '*'
    app_headers_allow_credentials = 'true'
    app_headers_allow_headers = '*'
    app_headers_allow_methods = '*'
    mongo_user = ''
    mongo_pass = ''
    mongo_host = 'localhost'
    mongo_port = 27017
    mongo_db_name = 'varvara'
    mongo_drop_table_on_init = False
    user_primary_fields = ['username']

    def __init__(self, **options):
        settings.update(options)

        for key, value in settings.items():
            setattr(self, key, value)