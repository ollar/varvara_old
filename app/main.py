from aiohttp import web
import uvloop
import asyncio
import logging

from .configs import Configs
from .middlewares import cors_middleware
from .db import initDB
from .routes import getRoutes
from .adapters.app import AppAdapter

asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

logging.basicConfig(level=logging.INFO)

async def make_app(**options):
    configs = Configs(**options)
    app = web.Application(
        middlewares=[
            cors_middleware
        ]
    )
    app['configs'] = configs
    app.add_routes(getRoutes())

    app.on_startup.append(on_startup)
    app.on_cleanup.append(on_cleanup)

    return app

async def on_startup(app):
    logging.info('App is starting up')

    (mongo_client, db) = await initDB(app['configs'])
    app['adapter'] = AppAdapter(db, configs=app['configs'])
    app['mongo_client'] = mongo_client

async def on_cleanup(app):
    logging.info('App is cleaning up')
    app['mongo_client'].close()
