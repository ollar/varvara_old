from .fixtures import client

async def test_smoke(client):
    """
    Tests app simple url is accessible
    """
    resp = await client.get('/')
    assert resp.status == 404