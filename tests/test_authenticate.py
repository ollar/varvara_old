from .fixtures import client
from json import dumps
from app.errors import WRONG_CREDENTIALS, REQUIRED_FIELDS_NOT_SPECIFIED

user = {
    "username": "test",
    "password": "test123"
}

wrong_user_pass = {
    "username": "test",
    "password": "test1233"
}


async def test_authenticate_empty_data(client):
    resp = await client.post('/authenticate', data=dumps({}))
    assert resp.status == 400


async def test_user_authenticate_correctly(client):
    # register user
    resp = await client.post('/register', data=dumps(user))
    assert resp.status == 200

    # login
    resp = await client.post('/authenticate', data=dumps(user))
    assert resp.status == 200
    assert 'jwt' in await resp.json()


async def test_authenticate_without_required_field(client):
    _user = dict(user)
    # register user
    resp = await client.post('/register', data=dumps(_user))
    assert resp.status == 200

    # login
    del _user['username']
    resp = await client.post('/authenticate', data=dumps(_user))
    assert resp.status == 400
    assert REQUIRED_FIELDS_NOT_SPECIFIED in await resp.text()


async def test_authenticate_wrong_user_should_reject(client):
    # register user
    resp = await client.post('/register', data=dumps(user))
    assert resp.status == 200

    resp = await client.post('/authenticate', data=dumps(wrong_user_pass))
    assert resp.status == 400
    assert WRONG_CREDENTIALS in await resp.text()