from json import dumps

from app.errors import REQUIRED_FIELDS_NOT_SPECIFIED, \
                        PASSWORD_TOO_SHORT, \
                        DUPLICATE_KEY_ERROR

from .fixtures import client


user = {
    "username": "test",
    "password": "pass"
}

async def test_register_not_specified_required_fields(client):
    resp = await client.post('/register', data=dumps({}))
    assert resp.status == 400
    assert REQUIRED_FIELDS_NOT_SPECIFIED in await resp.text()


async def test_register_user_no_pass_should_fail(client):
    user = {
        "username": "test",
    }

    resp = await client.post('/register', data=dumps(user))
    assert resp.status == 400
    assert REQUIRED_FIELDS_NOT_SPECIFIED in await resp.text()


async def test_register_user_short_pass_should_fail(client):
    user = {
        "username": "test",
        "password": "1"
    }

    resp = await client.post('/register', data=dumps(user))
    assert resp.status == 400
    assert PASSWORD_TOO_SHORT in await resp.text()


async def test_register_user(client):
    resp = await client.post('/register', data=dumps(user))
    assert resp.status == 200


async def test_register_user_with_duplicated_primary_field(client):
    resp = await client.post('/register', data=dumps(user))
    assert resp.status == 200

    resp = await client.post('/register', data=dumps(user))
    assert resp.status == 400
    assert DUPLICATE_KEY_ERROR in await resp.text()

async def test_successful_register_should_return_jwt(client):
    resp = await client.post('/register', data=dumps(user))
    assert resp.status == 200

    resp_json = await resp.json()
    assert 'jwt' in resp_json.keys()
