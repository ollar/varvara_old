from json import dumps

from app.errors import REQUIRED_FIELDS_NOT_SPECIFIED, \
                        PASSWORD_TOO_SHORT, \
                        DUPLICATE_KEY_ERROR

from .fixtures import client


"""
* Users are redirected to request their GitHub identity
* Users are redirected back to your site by GitHub
* Your app accesses the API with the user's access token



1. Request a user's GitHub identity

When your GitHub App specifies a login parameter, it prompts users with a specific account they can use for signing in and authorizing your app.

GET https://github.com/login/oauth/authorize
**
client_id
redirect_uri
scope?
state

https://github.com/login/oauth/authorize?client_id=Iv1.53abb948ef954ac1&redirect_uri=http://localhost:8080/handle-oauth2-code&state=qweqweqwe

receives ?code=d39672115baae8334d79&state=qweqweqwe


2. Users are redirected back to your site by GitHub

If the user accepts your request, GitHub redirects back to your site with a temporary code in a code parameter as well as the state you provided in the previous step in a state parameter. The temporary code will expire after 10 minutes. If the states don't match, then a third party created the request, and you should abort the process.

POST https://github.com/login/oauth/access_token
**
client_id
client_secret
code
redirect_uri
state
**

receives access_token=e72e16c7e42f292c6912e7710c838347ae178b4a&token_type=bearer


3. Use the access token to access the API

The access token allows you to make requests to the API on a behalf of a user.

Authorization: token OAUTH-TOKEN
GET https://api.github.com/user
"""


def test_oauth2(client):
    resp = await client.post('/authorize-oauth2', data=dumps({
        "provider": "github"
    }))
    # app should save users access token and pull user info

    assert resp.status == 200
    resp_json = await resp.json()

    assert resp_json["email"]