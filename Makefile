SHELL := /bin/bash

stop_mongo:
	-docker stop varvara || true && docker rm varvara || true

start_mongo: stop_mongo
	docker run -d -p 27017:27017 --name varvara mongo

check_mongo_running:
	docker inspect -f '{{.State.Running}}' varvara

dev:
	python run_dev.py

prod:
	gunicorn 'app.main:make_app' –-workers=3 --bind 0.0.0.0:8080 --worker-class aiohttp.worker.GunicornWebWorker

test:
	ENV='test' pytest -rs

ci-test: start_mongo
	make test


.PHONY: dev